package com.efsol.util;

import java.io.InputStream;
import java.io.Reader;

/**
 An Iterator to iterate through the "lines" of a stream or reader,
 skipping "blank" lines and comments..

 Comments are identified as any line whch starts with a supplied prefix
 (typically "#" or "rem" etc.)

 Typically used with a FileReader or FileInputStream.

 @author Frank Carver
*/
public class SkipBlankAndCommentLineIterator extends SkipBlankLineIterator
{
	private String prefix;

	public SkipBlankAndCommentLineIterator(Reader in, boolean autoclose, String prefix)
	{
		super(in, autoclose);
		this.prefix = prefix;
	}

	public SkipBlankAndCommentLineIterator(Reader in, String prefix)
	{
		super(in);
		this.prefix = prefix;
	}

	public SkipBlankAndCommentLineIterator(InputStream in, boolean autoclose, String prefix)
	{
		super(in, autoclose);
		this.prefix = prefix;
	}

	public SkipBlankAndCommentLineIterator(InputStream in, String prefix)
	{
		super(in);
		this.prefix = prefix;
	}

	@Override
	protected boolean isBlank(String line)
	{
		return (prefix != null && line.startsWith(prefix)) || line.trim().length() == 0;
	}
}
