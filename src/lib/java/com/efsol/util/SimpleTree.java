package com.efsol.util;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("rawtypes")
public class SimpleTree extends EmptyTree
	implements MutableTree
{
	protected Tree parent;
	protected Collection children;
	protected Object value;

	public SimpleTree()
	{
		parent = null;
		children = null;
		value = null;
	}

	public SimpleTree(Tree parent, Collection children, Object value)
	{
		this.parent = parent;
		this.children = children;
		this.value = value;
	}

	@Override
	public Tree getParent()
	{
		return parent;
	}

	@Override
	public Object getValue()
	{
		return value;
	}

	@Override
	public Collection getChildren()
	{
		return children;
	}

	@Override
	public void setParent(Tree parent)
	{
		this.parent = parent;
	}

	private void ensureChildren()
	{
		if (children == null)
		{
			children = new ArrayList();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void addChild(Tree child)
	{
		ensureChildren();
		children.add(child);
	}

	@Override
	public void removeChild(Tree child)
	{
		if (children != null)
		{
			children.remove(child);
		}
	}

	@Override
	public void setValue(Object value)
	{
		this.value = value;
	}
}
