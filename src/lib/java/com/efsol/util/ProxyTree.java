package com.efsol.util;

import java.util.Collection;

@SuppressWarnings("rawtypes")
public class ProxyTree extends SimpleTree
{
	public ProxyTree()
	{
		super();
	}

	public ProxyTree(Tree parent, Collection children, Object value)
	{
		super(parent, children, value);
	}

	@Override
	public Object getValue()
	{
		Object ret = value;
		if (ret != null && ret instanceof Proxy)
		{
			ret = ((Proxy)ret).getValue();
		}

		return ret;
	}

	@Override
	public void setValue(Object value)
	{
		if (this.value != null && this.value instanceof Proxy)
		{
			if (this.value instanceof MutableProxy)
			{
				((MutableProxy)this.value).setValue(value);
			}
			else
			{
				throw new UnsupportedOperationException("This proxy object is read-only");
			}
		}
		else
		{
			this.value = value;
		}
	}
}
