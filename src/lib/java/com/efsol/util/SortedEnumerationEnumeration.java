package com.efsol.util;

import java.util.Enumeration;
import java.util.Vector;

/**
 An Enumeration which is a sorthed version of another Enumeration
*/
@SuppressWarnings({"rawtypes", "unchecked"})
public class SortedEnumerationEnumeration implements Enumeration
{
	Vector elements;
	Enumeration enumeration;

	public SortedEnumerationEnumeration(Enumeration enumeration)
	{
		elements = new Vector();

		if (enumeration != null)
		{
			while(enumeration.hasMoreElements())
			{
				elements.addElement(enumeration.nextElement());
			}
		}
		Sorter.sortVector(elements);

		this.enumeration = elements.elements();
	}

	@Override
	public boolean hasMoreElements()
	{
		return enumeration.hasMoreElements();
	}

	@Override
	public Object nextElement()
	{
		return enumeration.nextElement();
	}
}