package com.efsol.util;

import java.util.Collection;

@SuppressWarnings("rawtypes")
public class EmptyTree implements Tree
{
	@Override
	public Tree getParent()
	{
		return null;
	}

	@Override
	public Object getValue()
	{
		return null;
	}

	@Override
	public Collection getChildren()
	{
		return null;
	}

	@Override
	public boolean equals(Object other)
	{
		return (other instanceof Tree) ? Trees.equals(this, (Tree)other) : false;
	}
}
