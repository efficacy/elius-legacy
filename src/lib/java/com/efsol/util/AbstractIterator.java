package com.efsol.util;

import java.util.Iterator;

@SuppressWarnings("rawtypes")
public abstract class AbstractIterator implements Iterator
{
	@Override
	public abstract boolean hasNext();
	@Override
	public abstract Object next();

    @Override
	public void remove()
    {
        throw new UnsupportedOperationException(this.getClass().getName() + " doesn't support remove");
    }
}
