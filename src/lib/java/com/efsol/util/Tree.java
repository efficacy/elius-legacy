package com.efsol.util;

import java.util.Collection;

@SuppressWarnings("rawtypes")
public interface Tree
{
	public Tree getParent();
	public Collection getChildren();
	public Object getValue();
}
