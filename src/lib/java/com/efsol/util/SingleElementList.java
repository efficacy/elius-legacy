package com.efsol.util;

import java.util.AbstractList;

@SuppressWarnings("rawtypes")
public class SingleElementList extends AbstractList
{
    private Object element;

    public SingleElementList(Object element)
    {
        this.element = element;
    }

    @Override
	public Object get(int index)
    {
        return element;
    }

    @Override
	public int size()
    {
        return 1;
    }
}
