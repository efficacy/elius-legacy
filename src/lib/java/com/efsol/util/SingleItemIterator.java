package com.efsol.util;

import java.util.NoSuchElementException;

public class SingleItemIterator extends AbstractIterator
{
    private final Object item;
    private boolean done;

    public SingleItemIterator(Object item)
    {
        this.item = item;
        done = false;
    }

    @Override
	public boolean hasNext()
    {
        return ! done;
    }

    @Override
	public Object next()
    {
    	if (!hasNext())
    	{
    		throw new NoSuchElementException();
    	}

    	done = true;
        return item;
    }
}
