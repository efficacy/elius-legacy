package com.efsol.util;

import java.util.Iterator;
import java.util.Vector;

/**
 An Iterator which is a sorted version of another Iterator
*/
@SuppressWarnings({"rawtypes", "unchecked"})
public class SortedIteratorIterator extends AbstractIterator
{
	private Vector elements;
	private Iterator it;

	public SortedIteratorIterator(Iterator it)
	{
		elements = new Vector();

		if (it != null)
		{
			while(it.hasNext())
			{
				elements.addElement(it.next());
			}
		}
		Sorter.sortVector(elements);

		this.it = elements.iterator();
	}

	@Override
	public boolean hasNext()
	{
		return it.hasNext();
	}


	@Override
	public Object next()
	{
		return it.next();
	}
}