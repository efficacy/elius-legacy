package com.efsol.util;

import java.util.Map;

/**
 An Iterator through the keys of a map, in order
*/
@SuppressWarnings("rawtypes")
public class SortedMapIterator extends SortedIteratorIterator
{
    private Map map;

	public SortedMapIterator(Map map)
	{
        super(map != null ? map.keySet().iterator() : null);
        this.map = map;
    }

	public Object nextValue()
	{
		return map.get(next());
	}
}
