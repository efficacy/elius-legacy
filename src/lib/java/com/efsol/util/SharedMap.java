package com.efsol.util;

import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("rawtypes")
class SharedMapEntry
	implements Map.Entry
{
	private Object key;
	private Object value;

	public SharedMapEntry(Object key, Object value)
	{
		this.key = key;
		this.value = value;
	}

	@Override
	public Object getKey()
	{
		return key;
	}

	@Override
	public Object getValue()
	{
		return value;
	}

	@Override
	public Object setValue(Object value)
	{
		throw new UnsupportedOperationException("This is a read-only Map");
	}

    @Override
	public boolean equals(Object other)
    {
		boolean ret = false;

		if (other instanceof Map.Entry)
		{
			Map.Entry entry = (Map.Entry)other;
			ret = this.key.equals(entry.getKey()) && this.value.equals(entry.getValue());
		}

        return ret;
    }

    @Override
	public int hashCode()
    {
		return (getKey()==null ? 0 : getKey().hashCode()) ^ (getValue()==null ? 0 : getValue().hashCode());
	}
}

class SharedMapIterator extends DelegatedIterator
{
	private SharedMap map;

    public SharedMapIterator(SharedMap map)
	{
		super(map.keySet().iterator());
		this.map = map;
	}

    @Override
	public Object next()
    {
        Object key = it.next();
		return new SharedMapEntry(key, map.get(key));
    }

    @Override
	public void remove()
    {
		throw new UnsupportedOperationException("This is a read-only Map");
    }
}

@SuppressWarnings("rawtypes")
class SharedMapEntrySet extends AbstractSet
{
	private SharedMap map;

	public SharedMapEntrySet(SharedMap map)
	{
		this.map = map;
	}

    @Override
	public int size()
	{
		return map.size();
	}

    @Override
	public boolean isEmpty()
	{
		return map.isEmpty();
	}

    @Override
	public Iterator iterator()
	{
		return new SharedMapIterator(map);
	}

    @Override
	public boolean add(Object o)
	{
		throw new UnsupportedOperationException("This is a read-only Map");
	}

    @Override
	public boolean addAll(Collection c)
	{
		throw new UnsupportedOperationException("This is a read-only Map");
	}

    @Override
	public void clear()
	{
		throw new UnsupportedOperationException("This is a read-only Map");
	}

    @Override
	public boolean remove(Object o)
	{
		throw new UnsupportedOperationException("This is a read-only Map");
	}

    @Override
	public boolean retainAll(Collection c)
	{
		throw new UnsupportedOperationException("This is a read-only Map");
	}
}

@SuppressWarnings("rawtypes")
public class SharedMap extends DelegatedMap
{
	private Object[] values;

    public SharedMap(Map map, Object[] values)
    {
        super(map);
		setValues(values);
    }

    @Override
	public boolean containsValue(Object value)
    {
		boolean ret = false;

		for (int i = 0; i < values.length; ++i)
		{
			if (value.equals(values[i]))
			{
				ret = true;
				break;
			}
		}

        return ret;
    }

    @Override
	public Set entrySet()
    {
		return new SharedMapEntrySet(this);
    }

    @Override
	public Collection values()
    {
        return Arrays.asList(values);
    }

    protected int getColumn(Object key)
    {
		int ret = -1;

		Object index = ((Map)getOther()).get(key);
		if (index != null && index instanceof Number)
		{
			ret = ((Number)index).intValue();
		}

		return ret;
    }

    @Override
	public Object get(Object key)
    {
		Object ret = null;
		int col = getColumn(key);

		if (col != -1)
		{
			ret = values[col];
		}

		return ret;
    }

    @Override
	public boolean equals(Object other)
    {
		boolean ret = false;

		if (other instanceof Map)
		{
			Map map = (Map)other;
			ret = this.entrySet().equals(map.entrySet());
		}

        return ret;
    }

    @Override
	public void clear()
    {
		throw new UnsupportedOperationException("This is a fixed-geometry Map");
    }

    @Override
	public Object put(Object key, Object value)
    {
		int col = getColumn(key);
		Object ret = null;

		if (col != -1)
		{
			ret = values[col];
			values[col] = value;
		}
		else
		{
			throw new ArrayIndexOutOfBoundsException("no such key '" + key + "'");
		}

		return ret;
    }

    @Override
	public Object remove(Object key)
    {
		throw new UnsupportedOperationException("This is a fixed-geometry Map");
    }

    @Override
	public int hashCode()
    {
		return entrySet().hashCode();
    }

	public void setValues(Object[] values)
	{
		this.values = values;
	}

	public Object[] getValues()
	{
		return values;
	}

    @Override
	public String toString()
	{
		int max = size() - 1;
		StringBuffer buf = new StringBuffer();
		Iterator it = entrySet().iterator();

		buf.append("{");
		for (int i = 0; i <= max; ++i)
		{
		    Entry e = (Entry) (it.next());
		    buf.append(e.getKey() + "=" + e.getValue());
		    if (i < max)
			{
				buf.append(", ");
			}
		}

		buf.append("}");
		return buf.toString();
    }
}
