package com.efsol.util;

import java.util.List;

@SuppressWarnings("rawtypes")
public class ListIndexIterator extends AbstractIterator
{
	private int max;
	private int current;

	public ListIndexIterator(List list)
	{
		current = 0;
		max = list.size();
	}

	@Override
	public boolean hasNext()
	{
        return current < max;
	}

	@Override
	public Object next()
	{
        return hasNext() ? new Integer(current++) : null;
	}
}