package com.efsol.util;

import java.util.NoSuchElementException;

public class EmptyIterator extends AbstractIterator
{
    private static EmptyIterator it = null;

    @Override
	public boolean hasNext()
    {
        return false;
    }

	@Override
	public Object next()
	{
		throw new NoSuchElementException();
	}

    public static synchronized EmptyIterator it()
    {
        if (it == null)
        {
            it = new EmptyIterator();
        }
        return it;
    }
}
