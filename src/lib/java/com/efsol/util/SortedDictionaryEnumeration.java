package com.efsol.util;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/** An emumeration of the keys of a Dictionary, sorted by key
*/
@SuppressWarnings({"rawtypes", "unchecked"})
public class SortedDictionaryEnumeration implements Enumeration
{
	Dictionary dictionary;
	Vector keys;
	Enumeration keyEnumeration;

	public SortedDictionaryEnumeration(Dictionary dictionary)
	{
		if (dictionary == null)
		{
			dictionary = new Hashtable();
		}
		this.dictionary = dictionary;

		keys = new Vector(dictionary.size());

		Enumeration e = dictionary.keys();
		while(e.hasMoreElements())
		{
			keys.addElement(e.nextElement());
		}

		Sorter.sortVector(keys);

		keyEnumeration = keys.elements();
	}

	@Override
	public boolean hasMoreElements()
	{
		return keyEnumeration.hasMoreElements();
	}

	public String nextKey()
	{
		return (String)keyEnumeration.nextElement();
	}

	public Object nextValue()
	{
		return dictionary.get(keyEnumeration.nextElement());
	}

	@Override
	public Object nextElement()
	{
		return nextKey();
	}

	public static String dump(Dictionary dictionary)
	{
		SortedDictionaryEnumeration e = new SortedDictionaryEnumeration(dictionary);
		String ret = "[ ";
		while (e.hasMoreElements())
		{
			String key = e.nextKey();
			ret += "'" + key + "'='" + dictionary.get(key) + "' ";
		}
		ret += "]";
		return ret;
	}
}