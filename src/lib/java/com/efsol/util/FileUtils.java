package com.efsol.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class FileUtils
{
    public static String readFile(InputStream in)
        throws IOException
    {
        StringBuffer ret = new StringBuffer();
        BufferedInputStream fin = new BufferedInputStream(in);

        for (int c = fin.read(); c != -1; c = fin.read())
        {
            ret.append((char)c);
        }

        return ret.toString();
    }

    public static String readFile(File filename)
    {
        String ret = "";
        try
        {
            FileInputStream fin = new FileInputStream(filename);
            ret = readFile(fin);
            fin.close();
        }
        catch(IOException ioe)
		{
			// ioe.printStackTrace();
		}

        return ret;
    }

    public static String readFile(File root, String filename)
    {
        return readFile(new File(root, filename));
    }

    public static String readFile(String root, String filename)
    {
        return readFile(new File(new File(root), filename));
    }

    public static String readFile(String filename)
    {
        return readFile(new File(filename));
    }

    public static void writeFile(Writer out, String content)
        throws IOException
    {
    	out.write(content);
    	out.flush();
    }

    public static void writeFile(OutputStream out, String content)
        throws IOException
    {
    	writeFile(new OutputStreamWriter(out), content);
    }

    public static void writeFile(String fileName, String content)
        throws IOException
    {
    	Writer out = new FileWriter(fileName);
    	writeFile(out, content);
    	out.close();
    }

    public static String readResource(Object self, String filename)
    {
        String ret = "";
        try
        {
			InputStream in = self.getClass().getClassLoader().getResourceAsStream(filename);
            ret = readFile(in);
            in.close();
        }
        catch(IOException ioe)
		{
			ioe.printStackTrace();
		}

        return ret;
    }


	static Object dummy = new FileUtils();

	// for use during constructors, when "this" is not available
	//
	// makes a guess that a dummy object is in the same classloader as the caller
	// this assumprion may be somewhat dodgy, use with care ...
	//
    public static String readResource(String filename)
    {
		return readResource(dummy, filename);
    }
}
