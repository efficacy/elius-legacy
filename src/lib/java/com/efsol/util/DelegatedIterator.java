package com.efsol.util;

import java.util.Iterator;

/**
 A base class for "iterator-helpers" which wrap the
 basic Iterator functionality, and add type-specific
 behaviour.
*/
@SuppressWarnings("rawtypes")
public class DelegatedIterator implements Iterator
{
    protected Iterator it;

    public DelegatedIterator(Iterator it)
    {
        this.it = it;
    }

    @Override
	public boolean hasNext()
    {
        return it.hasNext();
    }

    @Override
	public Object next()
    {
        return it.next();
    }

    @Override
	public void remove()
    {
        it.remove();
    }
}
