package com.efsol.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 An Iterator to iterate through the "lines" of a stream or reader.

 Typically used with a FileReader or FileInputStream

 @author Frank Carver
*/
public class LineIterator extends AbstractIterator
{
	protected BufferedReader reader = null;
	protected String line = null;
	protected boolean autoclose = true;

	public LineIterator(Reader in, boolean autoclose)
	{
		setup(in, autoclose);
	}

	public LineIterator(Reader in)
	{
		setup(in, true);
	}

	public LineIterator(InputStream in, boolean autoclose)
	{
		setup(new InputStreamReader(in), autoclose);
	}

	public LineIterator(InputStream in)
	{
		setup(new InputStreamReader(in), true);
	}

	public LineIterator(File in, boolean autoclose)
	{
		try
		{
			setup(new FileReader(in), autoclose);
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	public LineIterator(File in)
	{
		try
		{
			setup(new FileReader(in), true);
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}


	private void setup(Reader in, boolean autoclose)
	{
		reader = new BufferedReader(in);
		this.autoclose = autoclose;
		update();
	}

	@Override
	public boolean hasNext()
	{
		return line != null;
	}

	@Override
	public Object next()
	{
		Object ret = line;
		update();

		return ret;
	}

	protected void update()
	{
		try
		{
			line = reader.readLine();
			if (line == null && autoclose)
			{
				reader.close();
			}
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
			line = null;
		}
	}
}
