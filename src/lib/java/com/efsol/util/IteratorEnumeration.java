package com.efsol.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;

@SuppressWarnings("rawtypes")
public class IteratorEnumeration implements Enumeration
{
	private final Iterator it;

	public IteratorEnumeration(Iterator it)
	{
		this.it = it;
	}

	@Override
	public boolean hasMoreElements()
	{
        return it.hasNext();
	}

	@Override
	public Object nextElement()
		throws NoSuchElementException
	{
        return it.next();
	}
}