package com.efsol.util;

import java.io.InputStream;
import java.io.Reader;

/**
 An Iterator to iterate through the "lines" of a stream or reader,
 skipping "blank" lines..

 Typically used with a FileReader or FileInputStream

 @author Frank Carver
*/
public class SkipBlankLineIterator extends LineIterator
{
	public SkipBlankLineIterator(Reader in, boolean autoclose)
	{
		super(in, autoclose);
	}

	public SkipBlankLineIterator(Reader in)
	{
		super(in);
	}

	public SkipBlankLineIterator(InputStream in, boolean autoclose)
	{
		super(in, autoclose);
	}

	public SkipBlankLineIterator(InputStream in)
	{
		super(in);
	}

	@Override
	protected void update()
	{
		super.update();
		while (hasNext() && isBlank(line))
		{
			super.update();
		}
	}

	protected boolean isBlank(String line)
	{
		return line.trim().length() == 0;
	}
}
