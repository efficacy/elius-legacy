package com.efsol.util;

public interface Proxy
{
	public Object getValue();
}