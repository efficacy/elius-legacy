package com.efsol.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Hashtable;

@SuppressWarnings({"rawtypes", "unchecked"})
public class Logger
{
	private class Category
	{
		Category parent;
		Boolean logging;

		Category(Category parent)
		{
			this.parent = parent;
			this.logging = null;
		}
	}

	public static final String ALL = "ALL";
	public static final String nl = System.getProperty("line.separator");

	private static String spaces = "                                                            ";

	private Hashtable categories;
	private Category root;
	private Writer out;

	public Logger(Writer out)
	{
		this.out = out;
		categories = new Hashtable();
		root = new Category(null);

		root.logging = Boolean.TRUE;
		categories.put("ALL", root);
	}

	public Logger(OutputStream out)
	{
		this(new OutputStreamWriter(out));
	}

	public Logger()
	{
		this(System.out);
	}

	private Category add(String name, Category parent)
	{
		Category ret = new Category(parent);
		categories.put(name, ret);
		return ret;
	}

	public void addCategory(String name, String parentName)
	{
		Category parent = (Category)categories.get(parentName);
		if (parent == null)
		{
			parent = root;
		}

		add(name, parent);
	}

    public boolean isLoggable(String name)
    {
    	Category category = (Category)categories.get(name);
    	if (category == null)
    	{
    		category = add(name, root);
    	}

    	while (category.logging == null)
    	{
    		category = category.parent;
    	}

    	return category.logging.booleanValue();
    }

    public void allowLogging(String name)
    {
		Category category = (Category)categories.get(name);
		if (category == null)
		{
			category = add(name, root);
		}

		category.logging = Boolean.TRUE;
    }

    public void preventLogging(String name)
    {
		Category category = (Category)categories.get(name);
		if (category == null)
		{
			category = add(name, root);
		}

		category.logging = Boolean.FALSE;
    }

	public void log(String category, String message)
	{
		if (isLoggable(category))
		{
			try
			{
				out.write("(" + category + ") " + message + nl);
			}
			catch(IOException e) {}
		}
	}

	public String indent(int level)
	{
		return spaces.substring(spaces.length() - level);
	}

	public void log(String category, String message, int level)
	{
		log(category, indent(level) + message);
	}

}