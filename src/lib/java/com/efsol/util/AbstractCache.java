package com.efsol.util;

import java.util.Date;

class CacheStatus extends Status
{
	public CacheStatus()
	{
		super(Cached.EMPTY);
	}

	public CacheStatus(int initial)
	{
		super(initial);
	}
}

public abstract class AbstractCache
	implements Cached
{
	protected CacheStatus status = new CacheStatus();
	protected Date timestamp = new Date(0);
	protected Object cache;

	public AbstractCache(Object cache)
	{
		this.cache = cache;
		markFull();
	}

	public AbstractCache()
	{
		this.cache = null;
		markEmpty();
	}

	@Override
	public int getCachedStatus()
	{
		return status.get();
	}

	protected Object getObject()
	{
		return cache;
	}

	@Override
	public Object getValue()
	{
		ensure();
		return cache;
	}

	@Override
	public java.util.Date getTimestamp()
	{
		return timestamp;
	}

	private synchronized void mark(int status, long stamp)
	{
		this.status.set(status);
		timestamp.setTime(stamp);
	}

	private void markFull()
	{
		mark(FULL, System.currentTimeMillis());
	}

	private void markEmpty()
	{
		mark(EMPTY, 0);
	}

	private void markLoading()
	{
		status.set(LOADING);
	}

	private void markError()
	{
		mark(ERROR, 0);
	}

	@Override
	public void load()
	{
		if (status.get() != LOADING && status.get() != ERROR)
		{
			markLoading();
			try
			{
				if (doLoad())
				{
					markFull();
				}
				else
				{
					markError();
				}
			}
			catch(Error e)
			{
				markError();
				throw(e);
			}
		}
	}

	@Override
	public void unload()
	{
		doUnload();
		markEmpty();
	}

	@Override
	public void ensure()
	{
		if (status.get() == EMPTY)
		{
			load();
		}
	}

	@Override
	public void reload()
	{
		unload();
		load();
	}


	protected abstract boolean doLoad();
	protected abstract void doUnload();
}