package com.efsol.util;

public interface MutableProxy extends Proxy
{
	public void setValue(Object value);
}