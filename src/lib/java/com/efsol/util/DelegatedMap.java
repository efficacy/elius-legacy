package com.efsol.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"rawtypes", "unchecked"})
public class DelegatedMap extends Delegator implements Map
{
    public DelegatedMap(Map map)
    {
        super(map);
    }

    @Override
	public void clear()
    {
        ((Map)getOther()).clear();
    }

    @Override
	public boolean containsKey(Object key)
    {
        return ((Map)getOther()).containsKey(key);
    }

    @Override
	public boolean containsValue(Object value)
    {
        return ((Map)getOther()).containsValue(value);
    }

    @Override
	public Set entrySet()
    {
        return ((Map)getOther()).entrySet();
    }

    @Override
	public boolean equals(Object other)
    {
        return ((Map)getOther()).equals(other);
    }

    @Override
	public int hashCode()
    {
        return ((Map)getOther()).hashCode();
    }

    @Override
	public boolean isEmpty()
    {
        return ((Map)getOther()).isEmpty();
    }

    @Override
	public Set keySet()
    {
        return ((Map)getOther()).keySet();
    }

    @Override
	public Collection values()
    {
        return ((Map)getOther()).values();
    }

    @Override
	public Object get(Object key)
    {
        return ((Map)getOther()).get(key);
    }

    @Override
	public int size()
    {
        return ((Map)getOther()).size();
    }

    @Override
	public void putAll(Map other)
    {
        ((Map)getOther()).putAll(other);
    }

    @Override
	public Object put(Object key, Object value)
    {
        return ((Map)getOther()).put(key, value);
    }

    @Override
	public Object remove(Object key)
    {
        return ((Map)getOther()).remove(key);
    }
}
