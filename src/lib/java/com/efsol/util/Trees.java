package com.efsol.util;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Iterator;

@SuppressWarnings("rawtypes")
public class Trees
{
    private static Tree empty = null;

    public static synchronized Tree empty()
    {
        if (empty == null)
        {
            empty = new EmptyTree();
        }
        return empty;
    }

	public static Tree getRoot(Tree tree)
	{
		Tree ret = tree;
		for (Tree up = tree.getParent(); up != null; up = tree.getParent())
		{
			ret = up;
		}

		return ret;
	}

	/*
	 note - don't be tempted to add a comparison for "parent", as it
	 recurses straight back down to here again!
	*/
	public static boolean equals(Tree t1, Tree t2)
	{
		return  Utils.same(t1.getValue(), t2.getValue()) &&
				Utils.same(t1.getChildren(), t2.getChildren());
	}

	public static void indent(PrintStream out, int level)
	{
		for (int i = 0; i < level; ++i)
		{
			out.print(' ');
		}
	}

	public static void dump(Tree t, PrintStream out, int level)
	{
		indent(out, level);
		out.println("Tree: '" + t + "' p='" + t.getParent() + "' v='" + t.getValue() + "'");
		Collection kids = t.getChildren();
		if (kids != null)
		{
			Iterator it = kids.iterator();
			while (it.hasNext())
			{
				dump((Tree)it.next(), out, level+1);
			}
		}
	}

	public static void dump(Tree t, PrintStream out)
	{
		dump(t, out, 0);
	}

	public static void linkParentChild(MutableTree parent, MutableTree child)
	{
		child.setParent(parent);
		parent.addChild(child);
	}
}
