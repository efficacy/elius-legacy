package com.efsol.util;

public interface MutableTree extends Tree
{
	public void setParent(Tree parent);
	public void addChild(Tree child);
	public void removeChild(Tree child);
	public void setValue(Object value);
}
