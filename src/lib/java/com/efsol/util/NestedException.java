package com.efsol.util;

@SuppressWarnings("serial")
public class NestedException extends Exception
{
    private Throwable root;

    public NestedException(String description, Throwable rootCause)
    {
        super(description);
        this.root = rootCause;
    }

    public NestedException(String description)
    {
        this(description, null);
    }

    public NestedException(Throwable root)
    {
        this("Generic Nested Exception", root);
    }

    public NestedException()
    {
        this((Throwable)null);
    }

    public Throwable getRootCause()
    {
        return root;
    }
}
