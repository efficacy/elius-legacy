package com.efsol.util;

public interface Cached extends Proxy
{
	public static final int FULL = 0;
	public static final int EMPTY = 1;
	public static final int LOADING = 2;
	public static final int ERROR = 3;

	public int getCachedStatus();
	public java.util.Date getTimestamp();

	public void load();		// load if not already LOADING or ERROR
	public void unload();	// clear data and set status to EMPTY
	public void ensure();	// load only if EMPTY
	public void reload();	// unload to clear ERROR status, then force a load
}