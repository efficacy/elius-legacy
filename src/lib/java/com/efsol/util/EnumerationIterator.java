package com.efsol.util;

import java.util.Enumeration;

@SuppressWarnings("rawtypes")
public class EnumerationIterator extends AbstractIterator
{
	private final Enumeration e;

	public EnumerationIterator(Enumeration e)
	{
		this.e = e;
	}

	@Override
	public boolean hasNext()
	{
        return e.hasMoreElements();
	}

	@Override
	public Object next()
	{
        return e.nextElement();
	}
}