package com.efsol.util;

import java.util.Enumeration;
import java.util.NoSuchElementException;

@SuppressWarnings("rawtypes")
public class ArrayEnumeration implements Enumeration
{
	private Object[] array;
	private int i;

	public ArrayEnumeration(Object[] array)
	{
		this.array = array;
		i = 0;
	}

	@Override
	public boolean hasMoreElements()
	{
		return array==null ? false : i < array.length;
	}

	@Override
	public Object nextElement()
		throws NoSuchElementException
	{
		if (!hasMoreElements())
		{
			throw new NoSuchElementException();
		}

		return array[i++];

	}
}