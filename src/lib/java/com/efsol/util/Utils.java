package com.efsol.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class Utils
{
	public static final boolean same(Object a, Object b)
	{
		return (a==null && b==null) || (a != null && a.equals(b));
	}

	public static void copyStream(InputStream in, OutputStream out)
		throws IOException
	{
		BufferedInputStream bin = new BufferedInputStream(in);

		for (int c = bin.read(); c >= 0; c = bin.read())
		{
			out.write(c);
		}

		bin.close();
	}

	private static final String hex = "0123456789abcdef";

	private static char decode(char hi, char lo)
	{
		char ret = '%';
		int hihex = hex.indexOf(Character.toLowerCase(hi));
		int lohex = hex.indexOf(Character.toLowerCase(lo));
		if (hihex != -1 && lohex != -1)
		{
			ret = (char)(hihex*16 + lohex);
		}

		return ret;
	}

	public static String URLDecode(String encoded)
	{
		StringBuffer ret = new StringBuffer();
		int len = encoded.length();
		char[] array = new char[len];
		encoded.getChars(0,len,array,0);

		for (int i = 0; i < len; ++i)
		{
		    char c = array[i];
		    if (c == '+')
		    {
		    	c = ' ';
		    }
		    else if ( c == '%' && i < len-2)
			{
				c = decode(array[++i], array[++i]);
			}

			ret.append(c);
		}

		return ret.toString();
	}

	public static String URLEncode(String raw)
	{
		StringBuffer ret = new StringBuffer();
		int len = raw.length();
		char[] array = new char[len];
		raw.getChars(0,len,array,0);

		for (int i = 0; i < len; ++i)
		{
		    char c = array[i];
		    if (Character.isLetterOrDigit(c))
			{
				ret.append(c);
			}
		    else if (c == ' ')
		    {
		    	ret.append('+');
		    }
			else
			{
				int lo = (c) & 0x0f;
				int hi = ((c) & 0xf0) >> 4;
				ret.append('%');
				ret.append(hex.charAt(hi));
				ret.append(hex.charAt(lo));
			}
		}

		return ret.toString();
	}

    public static int simpleIntValue(Object obj, int dfl)
    {
        int ret = dfl;
        if (obj != null)
        {
			if (obj instanceof Number)
			{
				ret = ((Number)obj).intValue();
			}
			else
			{
	            try
	            {
	                ret = Integer.parseInt(obj.toString());
	            }
	            catch(NumberFormatException e)
	            {
	            }
			}
        }

        return ret;
    }

	public static int simpleIntValue(Object s)
	{
		return simpleIntValue(s, 0);
	}

    public static long simpleLongValue(Object obj, long dfl)
    {
        long ret = dfl;
        if (obj != null)
        {
			if (obj instanceof Number)
			{
				ret = ((Number)obj).intValue();
			}
			else
			{
	            try
	            {
	                ret = Long.parseLong(obj.toString());
	            }
	            catch(NumberFormatException e)
	            {
	            }
			}
        }

        return ret;
    }

	public static long simpleLongValue(Object s)
	{
		return simpleLongValue(s, 0);
	}

    public static Date simpleDateValue(Object obj, DateFormat df, Date dfl)
    {
		Date ret = dfl;

        if (obj != null)
        {
			if (obj instanceof Date)
			{
				ret = (Date)obj;
			}
			else
			if (obj instanceof Calendar)
			{
				ret = ((Calendar)obj).getTime();
			}
			else
			{
	            try
	            {
	                return df.parse(obj.toString());
	            }
	            catch(ParseException e)
	            {
	            }
			}
        }

        return ret;
    }

    public static Date simpleDateValue(Object obj, String format, Date dfl)
    {
		return simpleDateValue(obj, new SimpleDateFormat(format), new Date());
    }

	public static Date simpleDateValue(Object s, DateFormat format)
	{
		return simpleDateValue(s, format, new Date());
	}

	public static Date simpleDateValue(Object s, String format)
	{
		return simpleDateValue(s, format, new Date());
	}

	private static final DateFormat dflFormat = new SimpleDateFormat("dd/MM/yyyy");

	public static Date simpleDateValue(Object s, Date dfl)
	{
		return simpleDateValue(s, dflFormat, dfl);
	}

	public static Date simpleDateValue(Object s)
	{
		return simpleDateValue(s, dflFormat, new Date());
	}

    public static boolean isBlank(String s)
    {

		return s == null || s.trim().equals("");
    }

	public static int countIterator(Iterator it)
	{
		int ret = 0;
		while (it.hasNext())
		{
			++ret;
			it.next();
		}

		return ret;
	}


	/**
	 Attempt to create an object of a named class, logging any errors if appropriate

	 @param className the full qualified name of the class
	 @param logStream the stream to log errors to, if they occur
	 @return the newly created object, or null if it couldn't be done
	*/
	public static Object createObject(String className, OutputStream log)
	{
		Object ret = null;
		PrintStream out = null;;

		if (log != null)
		{
			if (log instanceof PrintStream)
			{
				out = (PrintStream)log;
			}
			else
			{
				out = new PrintStream(log);
			}
		}

		try
		{
			ret = Class.forName(className).newInstance();
		}
		catch(ClassNotFoundException e)
		{
			if (out != null)
			{
				out.println("couldn't find class '" + className + "'");
			}
		}
		catch(IllegalAccessException e)
		{
			if (out != null)
			{
				out.println("couldn't access class '" + className + "'");
			}
		}
		catch(InstantiationException e)
		{
			if (out != null)
			{
				out.println("couldn't instantiate class '" + className + "'");
			}
		}

		return ret;
	}

	public static Object createObject(String className)
	{
		return createObject(className, null);
	}

	public static void sleep(int millis)
	{
		try
		{
			Thread.sleep(millis);
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
		}
	}

	public static void dumpMap(Map map, String title, OutputStream stream)
	{
		PrintStream out;

		if (stream != null)
		{
			if (stream instanceof PrintStream)
			{
				out = (PrintStream)stream;
			}
			else
			{
				out = new PrintStream(stream);
			}

			out.println("Map: start of [" + title + "] ---------------");
			Iterator keys = map.keySet().iterator();
			while (keys.hasNext())
			{
				Object key = keys.next();
				out.println("'" + key + "'->'" + map.get(key) + "'");
			}
			out.println("Map: end of   [" + title + "] ---------------");
		}
	}

	public static void dumpMap(Map map, String title)
	{
		dumpMap(map, title, System.out);
	}

	public static String safeString(Object obj)
	{
		String ret = null;

		if (obj != null)
		{
			ret = obj.toString();
		}

		return ret;
	}

	public static String nullToEmpty(Object obj)
	{
		String ret = "";

		if (obj != null)
		{
			ret = obj.toString();
		}

		return ret;
	}

	/**
	 @deprecated
	*/
	@Deprecated
	public static String nullToSpace(Object obj)
	{
		return nullToEmpty(obj);
	}

	public static String notNullPrefix(Object obj, String prefix)
	{
		String ret = "";

		if (obj != null)
		{
			ret = prefix + obj.toString();
		}

		return ret;
	}

	public static String notEmptyPrefix(Object obj, String prefix)
	{
		String ret = "";

		if (obj != null)
		{
			String s = obj.toString();
			if (s != null && !"".equals(s))
			{
				ret = prefix + obj.toString();
			}
		}

		return ret;
	}

	public static String stripNonNumerics(String value)
	{
		StringBuffer ret = new StringBuffer();

		int len = value.length();
		for (int i = 0; i < len; ++i)
		{
			char c = value.charAt(i);
			if (Character.isDigit(c))
			{
				ret.append(c);
			}
		}

		return ret.toString();
	}

	public static String showList(List list)
	{
		StringBuffer ret = new StringBuffer("[");
		Iterator it = list.iterator();
		while (it.hasNext())
		{
			Object obj = it.next();
			if (obj != null)
			{
				ret.append(obj.toString());
			}

			if (it.hasNext())
			{
				ret.append(",");
			}
		}
		ret.append("]");

		return ret.toString();
	}

	public static void whereAmI()
	{
		try
		{
			throw new Exception("You are Here");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void dumpException(Throwable e, PrintStream out)
	{
		while (e != null)
		{
			String message = e.getMessage();
			if (message != null)
			{
				out.println("Exception message is '" + message + "'");
			}
			out.println(e.toString());
			e.printStackTrace(out);

			if (e instanceof java.sql.SQLException)
			{
				e = ((java.sql.SQLException)e).getNextException();
			}
			else
			{
				e = null;
			}
		}
	}

	public static void dumpException(Throwable e)
	{
		dumpException(e, System.out);
	}
}
