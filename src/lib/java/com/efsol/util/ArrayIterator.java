package com.efsol.util;

import java.util.*;

public class ArrayIterator extends AbstractIterator
{
	private Object[] array;
	private int i;
	
	public ArrayIterator(Object[] array)
	{
		this.array = array;
		i = 0;
	}

	public boolean hasNext()
	{
		return array!=null ? i < array.length : false;
	}

	public Object next()
		throws NoSuchElementException
	{
		if (!hasNext())
		{
			throw new NoSuchElementException();
		}

		return array[i++];
	}
}