package com.efsol.elius;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

import com.efsol.elius.parser.Parser;

@SuppressWarnings({"rawtypes", "unchecked"})
public class Interpreter extends BasicContext
{
	private boolean running;

	private static String prologue =
	" 'com.efsol.elius.words.ExitWord' primitive " +
	" 'com.efsol.elius.words.DefineWord' primitive " +
	" 'com.efsol.elius.words.VariableWord' primitive " +
	" 'com.efsol.elius.words.DoWord' primitive " +
	" 'com.efsol.elius.words.ExternalMethodWord' primitive " +
	" 'com.efsol.elius.words.ArrayWord' primitive " +
	" 'com.efsol.elius.words.StackWord' primitive " +
	" 'com.efsol.elius.words.IOWord' primitive " +

	" 'com.efsol.elius.words.MathWord' primitive " +
	" 'com.efsol.elius.words.StringWord' primitive " +

	" [ 'toString' native ] 'toString' define" +
	" [ 2 implode '' $join ] '$+' define " +
	" [ toString OUT 'write' arg_native] 'print' define " +
	" [ print '\n' print ] 'println' define ";

	public Interpreter(Context context)
	{
		super(context);
	}

	public Interpreter()
	{
		super();

		getWordMap().put("primitive", new com.efsol.elius.words.PrimitiveWord());
		run(prologue);
	}

	public void start()
	{
		running = true;
	}

	@Override
	public void stop()
	{
		running = false;
	}

	public boolean isRunning()
	{
		return running;
	}

	public void run()
	{
		Reader in = getReader();
		Parser parser = makeParser();
		parser.prime(in);
		start();

		Token token = parser.nextToken(in);

		while (isRunning() && token != null)
		{
System.out.println("--got token '" + token + "' isSignificant=" + token.isSignificant());
			if (token.isSignificant())
			{
				token.execute(this);
			}

			if (isRunning())
			{
				token = parser.nextToken(in);
			}
		}
	}

	public void run(Reader in, Writer out)
	{
		pushIOState(in, out);
		run();
		popIOState();
	}

	public void run(String text, Writer out)
	{
		StringReader in = new StringReader(text);
		run(in, out);
	}

	public void run(List tokens, Writer out)
	{
		Iterator it = tokens.iterator();

		start();
		while (isRunning() && it.hasNext())
		{
			Token token = (Token)it.next();

			if (token.isSignificant())
			{
				token.execute(this);
			}
		}
	}

	public String run(List tokens)
	{
		StringWriter out = new StringWriter();
		run(tokens, out);
		return out.toString();
	}

	public String run(String text)
	{
		StringReader in = new StringReader(text);
		StringWriter out = new StringWriter();
		run(in, out);
		return out.toString();
	}

	public static void main(String[] args)
	{
		Interpreter cli = new Interpreter();
		cli.run();
	}
}
