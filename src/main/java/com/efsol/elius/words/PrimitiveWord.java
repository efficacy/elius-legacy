package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;

import com.efsol.elius.Context;
import com.efsol.elius.Word;
import com.efsol.util.Utils;

@SuppressWarnings({"rawtypes", "unchecked"})
public class PrimitiveWord extends NamedWord
{
	public PrimitiveWord()
	{
		super("primitive");
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
		String cname = context.popString();
		Object obj = Utils.createObject(cname);

		Map words = context.getWordMap();
		if (obj != null && obj instanceof Word)
		{
			Word wordObj = (Word)obj;
			String[] names = wordObj.getNames();
			for (int i = 0; names != null && i < names.length; ++i)
			{
				words.put(names[i], obj);
			}
		}
	}
}
