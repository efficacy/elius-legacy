package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

import com.efsol.elius.Context;
import com.efsol.elius.Interpreter;
import com.efsol.elius.Word;
import com.efsol.elius.parser.ListTokenParser;

@SuppressWarnings("rawtypes")
public class SoftWord extends NamedWord
{
	private List tokens;

	public SoftWord(List tokens)
	{
		super((String[])null);
		this.tokens = tokens;
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
		Interpreter cli = new Interpreter(context);
		cli.run(tokens);
	}

	@Override
	public String toString()
	{
		return ListTokenParser.showList(tokens);
	}
}
