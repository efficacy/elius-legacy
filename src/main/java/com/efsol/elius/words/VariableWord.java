package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import java.util.Stack;

import com.efsol.elius.Context;
import com.efsol.elius.Word;

@SuppressWarnings({"rawtypes", "unchecked"})
public class VariableWord extends NamedWord
{
	public VariableWord()
	{
		super(new String[] { "get", "set" });
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
		Map vars = context.getVariableMap();
		Stack stack = context.getStack();

		if ("get".equals(word))
		{
			String name = context.popString();
			Object value = vars.get(name);
			if (value != null)
			{
				stack.push(value);
			}
		}

		else if ("set".equals(word))
		{
			String name = context.popString();
			Object value = stack.pop();
			if (value != null)
			{
				vars.put(name, value);
			}
		}
	}
}
