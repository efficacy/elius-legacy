package com.efsol.elius.words;

import java.io.IOException;
import com.efsol.elius.Context;
import com.efsol.elius.Word;

public abstract class NamedWord implements Word
{
	private String[] names = null;

	public NamedWord(String[] names)
	{
		this.names = names;
	}

	public NamedWord(String name)
	{
		this(new String[] { name });
	}
	
	/**
	 * @see Word#execute(Object, Context)
	 */
	public abstract void execute(Object word, Context context)
		throws IOException;

	/**
	 * @see Word#getNames()
	 */
	public String[] getNames()
	{
		return names;
	}
}
