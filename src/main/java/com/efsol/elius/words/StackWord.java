package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Stack;

import com.efsol.elius.Context;
import com.efsol.elius.Word;

@SuppressWarnings({"rawtypes", "unchecked"})
public class StackWord extends NamedWord
{
	private Stack temp;

	public StackWord()
	{
		super(new String[] { "dup", "swap", "drop", "roll" });
		temp = new Stack();
	}

	public void dup(Context context)
	{
		Object tos = context.pop();
		context.push(tos);
		context.push(tos);
	}

	public void swap(Context context)
	{
		Object a = context.pop();
		Object b = context.pop();
		context.push(a);
		context.push(b);
	}

	public void drop(Context context)
	{
		context.pop();
	}

	public void backup(Context context, int n)
	{
		for (int i = 0; i < n; ++i)
		{
			temp.push(context.pop());
		}
	}

	public void restore(Context context, int n)
	{
		for (int i = 0; i < n; ++i)
		{
			context.push(temp.pop());
		}
	}

	public void roll(Context context, int n)
	{
		int abs = Math.abs(n) - 1;
		if (n > 1)
		{
			backup(context, abs);
			Object mover = context.pop();
			restore(context, abs);
			context.push(mover);
		}
		else if (n < -1)
		{
			Object mover = context.pop();
			backup(context, abs);
			context.push(mover);
			restore(context, abs);
		}
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
		if ("dup".equals(word))
		{
			dup(context);
		}
		else if ("swap".equals(word))
		{
			swap(context);
		}
		else if ("drop".equals(word))
		{
			drop(context);
		}
		else if ("roll".equals(word))
		{
			int n = context.popInt();
			roll(context, n);
		}
	}
}
