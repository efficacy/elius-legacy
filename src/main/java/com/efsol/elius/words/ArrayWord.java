package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.efsol.elius.Context;
import com.efsol.elius.Word;

@SuppressWarnings("rawtypes")
public class ArrayWord extends NamedWord
{
	public ArrayWord()
	{
		super(new String[] { "implode", "explode" });
	}

	public void explode(Context context)
	{
		List list = context.popList();
		Iterator it = list.iterator();
		while (it.hasNext())
		{
			Object obj = it.next();
			context.push(obj);
		}
	}

	public void implode(Context context, int n)
	{
		Object[] ret = new Object[n];
		for (int i = n-1; i >= 0; --i)
		{
			ret[i] = context.pop();
		}

		context.pushContent(Arrays.asList(ret));
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
		if ("explode".equals(word))
		{
			explode(context);
		}
		else // implode
		{
			int n = context.popInt();
			implode(context, n);
		}
	}
}
