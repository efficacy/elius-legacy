package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Stack;

import com.efsol.elius.Context;
import com.efsol.elius.Word;

@SuppressWarnings("rawtypes")
public class MathWord extends NamedWord
{
	public MathWord()
	{
		super(new String[] { "+", "-", "*", "/" });
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word,  Context context)
		throws IOException
	{
		Stack stack = context.getStack();
		if (stack.size() < 2)
		{
			return;
		}

		int b = context.popInt();
		int a = context.popInt();
		int ret = 0;

		if ("+".equals(word))
		{
			ret = a + b;
		}
		else if ("-".equals(word))
		{
			ret = a - b;
		}
		else if ("*".equals(word))
		{
			ret = a * b;
		}
		else if ("/".equals(word))
		{
			ret = a / b;
		}

		context.pushContent(new Integer(ret));
	}
}
