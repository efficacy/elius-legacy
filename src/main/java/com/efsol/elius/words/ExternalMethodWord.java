package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import com.efsol.elius.Context;
import com.efsol.elius.Token;
import com.efsol.elius.Word;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ExternalMethodWord extends NamedWord
{
	public ExternalMethodWord()
	{
		super(new String[] { "native", "arg_native" });
	}

	public static final Object[] emptyArgs = new Object[] {};

	private Class[][] conv =
	{
		{ Byte.TYPE, Byte.class },
		{ Character.TYPE, Character.class },
		{ Character.TYPE, Byte.class },
		{ Integer.TYPE, Integer.class },
		{ Integer.TYPE, Character.class },
		{ Integer.TYPE, Byte.class }
	};

	private boolean isConvertible(Class dest, Class src)
	{
		if (dest.equals(src)) return true;
		if (dest.isAssignableFrom(src)) return true;

		for (int i = 0; i < conv.length; ++i)
		{
			if (conv[i][0].equals(dest) && conv[i][1].equals(src))
			{
				return true;
			}
		}

		return false;
	}

	private Method getMatchingMethod(Class c, String methodName, Class[] args)
		throws NoSuchMethodException
	{
		Method ret = null;

		Method[] methods = c.getMethods();
		for (int mi = 0; mi < methods.length; ++mi)
		{
			Method m = methods[mi];
			if (methodName.equals(m.getName()))
			{
				boolean convertible = true;

				Class[] params = m.getParameterTypes();
				if (params.length == args.length)
				{
					for (int pi = 0; pi < params.length; ++pi)
					{
						boolean isconv = isConvertible(params[pi], args[pi]);
						if (!isconv)
						{
							convertible = false;
						}
					}

					if (convertible)
					{
						ret = m;
						break;
					}
				}
			}
		}

		return ret; //c.getMethod(methodName, args);
	}

	public void executeExternalMethod(Context context, Object target, String methodName,
		Object[] args)
	{
		Object result = null;
		Class c = target.getClass();
		Class[] parameterTypes = new Class[args.length];
		for (int i = 0; i < args.length; ++i)
		{
			Object arg = args[i];
			if (arg instanceof Token)
			{
				args[i] = ((Token)arg).getContent();
			}

			Class ac = args[i].getClass();
			parameterTypes[i] = ac;
		}

		try
		{
			Method method = getMatchingMethod(c, methodName, parameterTypes);
			if (method != null)
			{
				if ("void".equals(method.getReturnType().toString()))
				{
					method.invoke(target, args);
				}
				else
				{
					result = method.invoke(target, args);
					context.pushContent(result);
				}
			}
			else
			{
				System.out.println("no method matching " + c + "." + methodName + parameterTypes);
			}
		}
		catch (NoSuchMethodException nsme)
		{
			nsme.printStackTrace();
		}
		catch (IllegalAccessException iae)
		{
			iae.printStackTrace();
		}
		catch (InvocationTargetException ite)
		{
			ite.printStackTrace();
		}
	}

	public void executeExternalMethod(Context context, Object target, String methodName)
	{
		executeExternalMethod(context, target, methodName, emptyArgs);
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
		String method = context.popString();
		Object target = context.popContent();

		if ("arg_native".equals(word))
		{
			Object arg = context.popContent();
			Object[] args;

			if (arg instanceof List)
			{
				args = ((List)arg).toArray();
			}
			else
			{
				args = new Object[] { arg };
			}

			executeExternalMethod(context, target, method, args);
		}
		else // "call"
		{
			executeExternalMethod(context, target, method);
		}
	}
}
