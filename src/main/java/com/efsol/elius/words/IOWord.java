package com.efsol.elius.words;

import java.io.IOException;
import java.io.File;

import com.efsol.util.FileUtils;

import com.efsol.elius.Context;

public class IOWord extends NamedWord
{
	public IOWord()
	{
		super(new String[] { "IN", "OUT", "readfile", "writefile", "dropfile" });
	}
	
	/**
	 * @see Word#execute(Object, Context)
	 */
	public void execute(Object word,Context context)
		throws IOException
	{
		if ("OUT".equals(word))
		{
			context.pushContent(context.getWriter());
		}
		else if ("OUT".equals(word))
		{
			context.pushContent(context.getReader());
		}
		else if ("readfile".equals(word))
		{
			context.pushContent(FileUtils.readFile(context.popString()));
		}
		else if ("writefile".equals(word))
		{
			FileUtils.writeFile(context.popString(), context.popString());
		}
		else if ("dropfile".equals(word))
		{
			File file = new File(context.popString());
			file.delete();
		}
	}
}
