package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.efsol.elius.Context;
import com.efsol.elius.Word;

@SuppressWarnings({"rawtypes", "unchecked"})
public class DefineWord extends NamedWord
{
	public DefineWord()
	{
		super(new String[] { "define", "edit" });
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word,  Context context)
		throws IOException
	{
		Map words = context.getWordMap();

		String wname = context.popString();
		if ("define".equals(word))
		{
			List def = context.popList();

			if (def != null && def.size() > 0)
			{
				words.put(wname, new SoftWord(def));
			}
		}
		else if ("edit".equals(word))
		{
			Word def = (Word)words.get(wname);
			context.pushContent(def.toString());
		}
	}
}
