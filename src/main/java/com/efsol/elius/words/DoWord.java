package com.efsol.elius.words;

import java.io.IOException;
import java.util.List;

import com.efsol.elius.Context;
import com.efsol.elius.Interpreter;
import com.efsol.elius.Word;

@SuppressWarnings("rawtypes")
public class DoWord extends NamedWord
{
	public DoWord()
	{
		super("do");
	}

	/**
	 * @see Word#execute(Object, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
			List tokens = context.popList();
			Interpreter cli = new Interpreter(context);
			cli.run(tokens);
	}
}
