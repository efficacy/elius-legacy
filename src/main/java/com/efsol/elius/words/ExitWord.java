package com.efsol.elius.words;

import java.io.IOException;
import com.efsol.elius.Context;

public class ExitWord extends NamedWord
{
	public ExitWord()
	{
		super("exit");
	}
	
	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	public void execute(Object word, Context context)
		throws IOException
	{
			context.stop();
	}
}
