package com.efsol.elius.words;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import com.efsol.elius.Context;
import com.efsol.elius.LiteralToken;
import com.efsol.elius.Token;
import com.efsol.elius.Word;

@SuppressWarnings({"rawtypes", "unchecked"})
public class StringWord extends NamedWord
{
	public StringWord()
	{
		super(new String[] { "$split", "$join" });
	}

	public List split(String sep, String text)
	{
		List list = new ArrayList();
		StringTokenizer tok = new StringTokenizer(text, sep);
		while (tok.hasMoreTokens())
		{
			list.add(new LiteralToken(tok.nextToken()));
		}
		return list;
	}

	public Object join(String sep, List words)
	{
		StringBuffer buf = new StringBuffer();

		Iterator it = words.iterator();
		String put = "";
		while (it.hasNext())
		{
			String text = ((Token)it.next()).getContent().toString();
			buf.append(put);
			buf.append(text);
			put = sep;
		}

		return buf.toString();
	}

	/**
	 * @see Word#execute(Object, Reader, Writer, Context)
	 */
	@Override
	public void execute(Object word, Context context)
		throws IOException
	{
		Object ret = "";

		if  ("$split".equals(word))
		{
			ret = split(context.popString(), context.popString());
		}
		else if ("$join".equals(word))
		{
			ret = join(context.popString(), context.popList());
		}

		context.pushContent(ret);
	}
}
