package com.efsol.elius.parser;

import java.io.Reader;
import java.util.Iterator;

import com.efsol.elius.Token;

@SuppressWarnings("rawtypes")
public class StandardParser implements Parser, ParserFactory
{
	private int prev;

	private static TokenParser[] tokens = new TokenParser[]
	{
		new NumberTokenParser(),
		new StringTokenParser(),
		new ListTokenParser(),
		new CommentTokenParser(),
		new SpaceTokenParser(),
		new WordTokenParser()
	};

	public StandardParser()
	{
		clear();
	}

	@Override
	public Parser makeParser()
	{
		return new StandardParser();
	}

	@Override
	public void clear()
	{
		prev = 0;
	}

	public TokenParser getToken(int c)
	{
		TokenParser ret = null;

		for (int i = 0; i < tokens.length; ++i)
		{
			TokenParser token = tokens[i];
			if (token.isInitial(c))
			{
				ret = token;
				break;
			}
		}

		return ret;
	}

	@Override
	public Token nextToken(Reader in)
	{
		TokenParser ret = null;

		if (prev == 0)
		{
			prime(in);
		}

		if (prev > 0)
		{
			char c = (char)prev;
			ret = getToken(c);

			if (ret != null)
			{
				prev = ret.parse(c, in, this);
			}
		}

		return ret != null ? ret.getToken() : null;
	}

	@Override
	public void prime(Reader in)
	{
		while (prev == 0)
		{
			try
			{
				prev = in.read();
			}
			catch(java.io.IOException ioe)
			{
				ioe.printStackTrace();
				prev = -1;
			}
		}
	}

	@Override
	public Iterator iterator(Reader in)
	{
		return new ParserIterator(this, in);
	}
}
