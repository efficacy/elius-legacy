package com.efsol.elius.parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.efsol.elius.Token;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ListTokenParser extends DelimitedTokenParser
{
	public ListTokenParser()
	{
		super('[', ']', '\\');
	}

	public static String showList(List list)
	{
		StringBuffer buf = new StringBuffer("[ ");
		Iterator it = list.iterator();
		while (it.hasNext())
		{
			Token token = (Token)it.next();
			buf.append(token);
			buf.append(" ");
		}
		buf.append("]");
		return buf.toString();
	}

	/**
	 * @see AbstractTokenParser#make(String)
	 */
	@Override
	protected Object make(String s, ParserFactory pf)
	{
		List ret = new ArrayList();
		Parser parser = pf.makeParser();
		StringReader in = new StringReader(s);

		parser.prime(in);
		Token token = parser.nextToken(in);
		while (token != null)
		{
			if (token.isSignificant())
			{
				ret.add(token);
			}

			token = parser.nextToken(in);
		}

		return ret;
	}
}
