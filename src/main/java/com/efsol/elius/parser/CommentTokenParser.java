package com.efsol.elius.parser;

import com.efsol.elius.Token;
import com.efsol.elius.SeparatorToken;

public class CommentTokenParser extends DelimitedTokenParser
{
	public CommentTokenParser()
	{
		super('(', ')');
	}

	/**
	 * @see AbstractTokenParser#make(String)
	 */
	protected Object make(String s, ParserFactory pf)
	{
		return null;
	}
	
	public Token getToken()
	{
		return new SeparatorToken(value);
	}
}
