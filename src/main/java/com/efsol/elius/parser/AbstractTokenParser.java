package com.efsol.elius.parser;

import java.io.Reader;

public abstract class AbstractTokenParser implements TokenParser
{
	protected Object value;

	/**
	 * @see Token#isContent()
	 */
	public abstract boolean isContent(int c, int pos);

	/**
	 * @see Token#isInitial()
	 */
	public boolean isInitial(int c)
	{
		return isContent(c, 0);
	}
	
	/**
	 * @see Token#isEscape(int)
	 */
	public boolean isEscape(int c)
	{
		return false;
	}

	/**
	 * @see Token#isFinal()
	 */
	public boolean isFinal(int c)
	{
		return false;
	}

	/**
	 * @see Token#getValue()
	 */
	public Object getContent()
	{
		return value;
	}

	/**
	 * @see Token#isSignificant()
	 */
	public boolean isSignificant()
	{
		return true;
	}

	protected abstract Object make(String s, ParserFactory pf);

	/**
	 * @see TokenParser#parse(int, Reader)
	 */
	public int parse(int c, Reader in, ParserFactory pf)
	{
		int pos = 0;
		StringBuffer buf = new StringBuffer();
		try
		{
			while (c >= 0 && isContent(c, pos))
			{
				buf.append((char)c);
				c = in.read();
				++pos;
			}
		}
		catch(java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		String s = buf.toString();

		value = make(s, pf);
		return c;
	}
	
	public String toString()
	{
		return value.toString();
	}
}
