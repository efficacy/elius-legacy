package com.efsol.elius.parser;

import java.io.Reader;

public abstract class DelimitedTokenParser extends LiteralTokenParser
{
	private int start;
	private int end;
	private int esc;
	
	public DelimitedTokenParser(int start, int end, int esc)
	{
		this.start = start;
		this.end = end;
		this.esc = esc;
	}
	
	public DelimitedTokenParser(int start, int end)
	{
		this(start, end, 0);
	}
	
	/**
	 * @see Token#isInitial(int)
	 */
	public boolean isInitial(int c)
	{
		return start == c;
	}

	/**
	 * @see Token#isFinal()
	 */
	public boolean isFinal(int c)
	{
		return end == c;
	}
	
	/**
	 * @see Token#isEscape(int)
	 */
	public boolean isEscape(int c)
	{
		return esc == c;
	}
	
	/**
	 * @see Token#isContent(int)
	 */
	public boolean isContent(int c, int pos)
	{
		return !isInitial(c) && !isFinal(c);
	}

	/**
	 * @see Token#parse(int, Reader)
	 */
	public int parse(int c, Reader in, ParserFactory pf)
	{
		int pos = 0;
		int nesting = 0;
		StringBuffer buf = new StringBuffer();
		
		try
		{
			if (c >= 0 && !isContent(c,pos))
			{
				c = in.read();
				++pos;
			}
			
			while (c >= 0)
			{
				boolean contentflag = isContent(c,pos);
				boolean initialflag = isInitial(c);
				boolean finalflag = isFinal(c);
				
				if (isEscape(c))
				{
					c = in.read();
					contentflag = true;
					initialflag = false;
					finalflag = false;
					++pos;
				}
				
				if (finalflag)
				{
					if (nesting > 0)
					{
						--nesting;
						contentflag = true;
					}
					else
					{
						c = in.read();
						break;
					}
				}
				
				if (initialflag)
				{
					++nesting;
					contentflag = true;
				}
				
				if (contentflag)
				{
					buf.append((char)c);
				}

				c = in.read();
				++pos;
			}
		}
		catch(java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}
		
		value = make(buf.toString(), pf);
		return c;
	}
}
