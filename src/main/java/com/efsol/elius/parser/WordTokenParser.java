package com.efsol.elius.parser;

import java.io.Reader;
import java.io.Writer;
import java.util.Map;

import com.efsol.elius.Context;
import com.efsol.elius.ExecutableToken;
import com.efsol.elius.Token;
import com.efsol.elius.Word;

@SuppressWarnings("rawtypes")
public class WordTokenParser extends AbstractTokenParser
{
	public static boolean isWordChar(int c)
	{
		return ! Character.isWhitespace((char)c);
	}
	/**
	 * @see AbstractTokenParser#isValid()
	 */
	@Override
	public boolean isContent(int c, int pos)
	{
		return isWordChar(c);
	}

	/**
	 * @see AbstractTokenParser#make()
	 */
	@Override
	protected Object make(String s, ParserFactory pf)
	{
		return s;
	}

	public void execute(Reader in, Writer out, Context context)
	{
		Map words = context.getWordMap();
		Word word = (Word)words.get(value);
		if (word != null)
		{
			try
			{
				word.execute(value, context);
			}
			catch (java.io.IOException ioe)
			{
				ioe.printStackTrace();
			}
		}
		else
		{
			System.out.println("Warning, unrecognized word '" + value + "' ignored");
		}
	}

	@Override
	public Token getToken()
	{
		return new ExecutableToken((String)value);
	}
}
