package com.efsol.elius.parser;

import java.io.Reader;
import com.efsol.elius.Token;

/**
 * @author frank
 */
public interface TokenParser 
{
	public boolean isInitial(int c);
	public boolean isEscape(int c);
	public boolean isFinal(int c);
	public boolean isContent(int c, int pos);
	
	public int parse(int c, Reader in, ParserFactory pf);
	public Token getToken();
}
