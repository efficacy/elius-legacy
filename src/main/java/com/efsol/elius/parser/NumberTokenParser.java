package com.efsol.elius.parser;

import com.efsol.elius.Token;
import com.efsol.elius.ExecutableToken;

public class NumberTokenParser extends LiteralTokenParser
{
	/**
	 * @see AbstractTokenParser#isValid()
	 */
	public boolean isContent(int c, int pos)
	{
		if (pos==0) 
		{
			return ((c=='-') || Character.isDigit((char)c));
		}
		else
		{
			return WordTokenParser.isWordChar(c);
		}
	}
	
	/**
	 * @see AbstractTokenParser#make()
	 */
	protected Object make(String s, ParserFactory pf)
	{
       	Object ret = s;
        try
        {
            int i = Integer.parseInt(s);
            ret = new Integer(i);
        }
        catch(NumberFormatException e)
        {
        }
        
        return ret;
    }
	
	public Token getToken()
	{
		if (value instanceof String)
		{
			return new ExecutableToken((String)value);
		}
		else
		{
			return super.getToken();
		}
	}
}
