package com.efsol.elius.parser;

import com.efsol.elius.Token;
import com.efsol.elius.LiteralToken;

public abstract class LiteralTokenParser extends AbstractTokenParser
{
	public Token getToken()
	{
		return new LiteralToken(value);
	}
}
