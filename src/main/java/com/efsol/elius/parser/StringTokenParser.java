package com.efsol.elius.parser;

public class StringTokenParser extends DelimitedTokenParser
{
	public StringTokenParser()
	{
		super('\'', '\'', '\\');
	}

	/**
	 * @see AbstractTokenParser#make(String)
	 */
	protected Object make(String s, ParserFactory pf)
	{
		return s;
	}

	/**
	 * @see Token#isLiteral()
	 */
	public boolean isLiteral()
	{
		return true;
	}
}
