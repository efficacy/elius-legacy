package com.efsol.elius.parser;

public interface ParserFactory
{
	public Parser makeParser();
}
