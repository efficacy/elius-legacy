package com.efsol.elius.parser;

public class SpaceTokenParser extends SeparatorTokenParser
{
	/**
	 * @see AbstractTokenParser#isContent()
	 */
	public boolean isContent(int c, int pos)
	{
		return Character.isWhitespace((char)c);
	}

	/**
	 * @see Token#isSignificant()
	 */
	public boolean isSignificant()
	{
		return false;
	}
	
	/**
	 * @see AbstractTokenParser#make()
	 */
	protected Object make(String s, ParserFactory pf)
	{
		return " ";
	}
}
