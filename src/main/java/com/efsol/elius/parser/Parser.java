package com.efsol.elius.parser;

import java.io.Reader;
import java.util.Iterator;

import com.efsol.elius.Token;

@SuppressWarnings("rawtypes")
public interface Parser
{
	public void clear();
	public void prime(Reader in);
	public Token nextToken(Reader in);

	public Iterator iterator(Reader in);
}
