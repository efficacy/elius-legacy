package com.efsol.elius.parser;

import com.efsol.elius.Token;
import com.efsol.elius.SeparatorToken;

public abstract class SeparatorTokenParser extends AbstractTokenParser
{
	public Token getToken()
	{
		return new SeparatorToken(value);
	}
}
