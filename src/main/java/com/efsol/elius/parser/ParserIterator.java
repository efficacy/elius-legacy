package com.efsol.elius.parser;

import java.io.Reader;
import java.util.Iterator;

import com.efsol.elius.Token;

@SuppressWarnings("rawtypes")
public class ParserIterator implements Iterator
{
	private Parser parser;
	private Reader in;
	private Token token;
	private boolean more;

	public ParserIterator(Parser parser, Reader in)
	{
		this.parser = parser;
		this.in = in;
		this.token = null;
		this.more = true;
	}

	private void ensure()
	{
		if (more && token == null)
		{
			token = parser.nextToken(in);
			if (token == null)
			{
				more = false;
			}
		}
	}

	/**
	 * @see Iterator#hasNext()
	 */
	@Override
	public boolean hasNext()
	{
		ensure();
		return more;
	}

	/**
	 * @see Iterator#next()
	 */
	@Override
	public Object next()
	{
		ensure();
		Object ret = token;
		token = null;
		return ret;
	}

	/**
	 * @see Iterator#remove()
	 */
	@Override
	public void remove()
	{
		throw new UnsupportedOperationException();
	}

}
