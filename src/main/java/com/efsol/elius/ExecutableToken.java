package com.efsol.elius;

import java.util.Map;

@SuppressWarnings("rawtypes")
public class ExecutableToken implements Token
{
	private String name;

	public ExecutableToken(String name)
	{
		this.name = name;
	}

	/**
	 * @see Token#getContent()
	 */
	@Override
	public Object getContent()
	{
		return name;
	}

	/**
	 * @see Token#isSignificant()
	 */
	@Override
	public boolean isSignificant()
	{
		return true;
	}

	/**
	 * @see Token#execute( Context)
	 */
	@Override
	public void execute(Context context)
	{
		Map words = context.getWordMap();
		Word word = (Word)words.get(name);
		if (word != null)
		{
			try
			{
				word.execute(name, context);
			}
			catch (java.io.IOException ioe)
			{
				ioe.printStackTrace();
			}
		}
		else
		{
			System.out.println("Warning, unrecognized word '" + name + "' ignored");
		}
	}

	@Override
	public String toString()
	{
		return name;
	}
}
