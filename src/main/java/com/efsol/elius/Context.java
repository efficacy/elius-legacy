package com.efsol.elius;

import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import com.efsol.elius.parser.Parser;
import com.efsol.elius.parser.ParserFactory;

@SuppressWarnings("rawtypes")
public interface Context
{
	public ParserFactory getParserFactory();
	public Parser makeParser();
	public Stack getStack();
	public Map getWordMap();
	public Map getVariableMap();

	public Reader getReader();
	public Writer getWriter();

	public void clear();
	public int size();

	public void push(Object obj);
	public void pushContent(Object obj);

	public Token pop();
	public Object popContent();
	public String popString();
	public int popInt();
	public List popList();

	public void pushIOState(Reader reader, Writer writer);
	public void popIOState();

	public void stop();
}
