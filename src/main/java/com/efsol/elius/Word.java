package com.efsol.elius;

import java.io.IOException;

public interface Word
{
	public void execute(Object word, Context context)
		throws IOException;
	public String[] getNames();
}
