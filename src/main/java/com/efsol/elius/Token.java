package com.efsol.elius;

public interface Token 
{
	public Object getContent();
	public boolean isSignificant();
	public void execute(Context context);
}
