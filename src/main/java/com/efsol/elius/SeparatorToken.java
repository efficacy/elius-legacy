package com.efsol.elius;

public class SeparatorToken implements Token
{
	private Object content;
	
	public SeparatorToken(Object content)
	{
		this.content = content;
	}
	
	public SeparatorToken()
	{
		this(" ");
	}
	
	/**
	 * @see Token#getContent()
	 */
	public Object getContent()
	{
		return content;
	}

	/**
	 * @see Token#isSignificant()
	 */
	public boolean isSignificant()
	{
		return false;
	}

	/**
	 * @see Token#execute(Reader, Writer, Context)
	 */
	public void execute(Context context)
	{
	}

}
