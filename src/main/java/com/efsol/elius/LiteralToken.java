package com.efsol.elius;

import java.io.Reader;
import java.io.Writer;
import java.util.List;

import com.efsol.elius.parser.ListTokenParser;
import com.efsol.util.Utils;

@SuppressWarnings({"rawtypes", "unchecked"})
public class LiteralToken implements Token
{
	private Object content;

	public LiteralToken(Object content)
	{
		this.content = content;
	}

	/**
	 * @see Token#getContent()
	 */
	@Override
	public Object getContent()
	{
		return content;
	}

	/**
	 * @see Token#isSignificant()
	 */
	@Override
	public boolean isSignificant()
	{
		return true;
	}

	/**
	 * @see Token#execute(Reader, Writer, Context)
	 */
	@Override
	public void execute(Context context)
	{
		context.getStack().push(this);
	}

	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		boolean ret = false;

		if (other instanceof LiteralToken)
		{
			LiteralToken otherToken = (LiteralToken)other;
			ret = 	Utils.same(content, otherToken.content);
		}

		return ret;
	}

	@Override
	public String toString()
	{
		String ret = "";

		if (content instanceof Integer)
		{
			ret = content.toString();
		}
		else if (content instanceof String)
		{
			ret = "'" + (String)content + "'";
		}
		else if (content instanceof List)
		{
			ret = ListTokenParser.showList((List)content);
		}

		return ret;
	}
}
