package com.efsol.elius;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import com.efsol.elius.parser.Parser;
import com.efsol.elius.parser.ParserFactory;
import com.efsol.elius.parser.StandardParser;
import com.efsol.util.Utils;

class IOPair
{
	public Reader reader;
	public Writer writer;
	public IOPair(Reader reader, Writer writer)
	{
		this.reader = reader;
		this.writer = writer;
	}
}

@SuppressWarnings({"rawtypes", "unchecked"})
public class BasicContext implements Context
{
	private Reader reader;
	private Writer writer;
	private Stack io;

	private ParserFactory parserFactory;
	private Stack data;
	private Map words;
	private Map variables;

	public BasicContext(Context context)
	{
		this.reader = context.getReader();
		this.writer = context.getWriter();
		this.io = new Stack();
		this.parserFactory = context.getParserFactory();
		this.data = context.getStack();
		this.words = context.getWordMap();
		this.variables = context.getVariableMap();
	}

	public BasicContext(Reader reader, Writer writer)
	{
		this.reader = reader;
		this.writer = writer;
		this.io = new Stack();
		parserFactory = new StandardParser();
		data = new Stack();
		words = new HashMap();
		variables = new HashMap();
	}

	public BasicContext()
	{
		this(new InputStreamReader(System.in), new OutputStreamWriter(System.out));
	}

	/**
	 * @see Context#getReader()
	 */
	@Override
	public Reader getReader()
	{
		return reader;
	}

	/**
	 * @see Context#getReader()
	 */
	@Override
	public Writer getWriter()
	{
		return writer;
	}

	/**
	 * @see Context#getParserFactory()
	 */
	@Override
	public ParserFactory getParserFactory()
	{
		return parserFactory;
	}

	/**
	 * @see Context#makeParser()
	 */
	@Override
	public Parser makeParser()
	{
		return parserFactory.makeParser();
	}

	/**
	 * @see Context#getStack()
	 */
	@Override
	public Stack getStack()
	{
		return data;
	}

	/**
	 * @see Context#getWordMap()
	 */
	@Override
	public Map getWordMap()
	{
		return words;
	}

	/**
	 * @see Context#getVariableMap()
	 */
	@Override
	public Map getVariableMap()
	{
		return variables;
	}

	/**
	 * @see Context#clear()
	 */
	@Override
	public void clear()
	{
		data.clear();
	}

	/**
	 * @see Context#size()
	 */
	@Override
	public int size()
	{
		return data.size();
	}

	/**
	 * @see Context#pop()
	 */
	@Override
	public Token pop()
	{
		return (Token)data.pop();
	}

	/**
	 * @see Context#popContent()
	 */
	@Override
	public Object popContent()
	{
		return pop().getContent();
	}

	/**
	 * @see Context#popString()
	 */
	@Override
	public String popString()
	{
		return popContent().toString();
	}

	/**
	 * @see Context#popInt()
	 */
	@Override
	public int popInt()
	{
		return Utils.simpleIntValue(popContent());
	}

	/**
	 * @see Context#popList()
	 */
	@Override
	public List popList()
	{
		return (List)popContent();
	}

	/**
	 * @see Context#push(Object)
	 */
	@Override
	public void pushContent(Object obj)
	{
		data.push(new LiteralToken(obj));
	}

	/**
	 * @see Context#push(Object)
	 */
	@Override
	public void push(Object obj)
	{
		data.push(obj);
	}

	@Override
	public void pushIOState(Reader reader, Writer writer)
	{
		io.push(new IOPair(this.reader, this.writer));
		this.reader = reader;
		this.writer = writer;
	}

	@Override
	public void popIOState()
	{
		IOPair pair= (IOPair)io.pop();
		this.reader = pair.reader;
		this.writer = pair.writer;
	}

	/**
	 * @see Context#stop()
	 */
	@Override
	public void stop()
	{
	}

}
