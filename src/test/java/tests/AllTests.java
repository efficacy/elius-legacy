package tests;

import junit.framework.*;

public class AllTests extends TestCase
{
	public AllTests(String name)
	{
		super(name);
	}
	
	public static TestSuite suite()
	{
		TestSuite ret = new TestSuite();

		ret.addTest(new TestSuite(ExternalMethodTest.class));
		ret.addTest(new TestSuite(InterpreterTest.class));

		ret.addTest(new TestSuite(EmptyTest.class));

		return ret;
	}
}
