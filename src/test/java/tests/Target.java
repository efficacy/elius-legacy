package tests;

public class Target
{
	public String arg;
	
	public void nowt()
	{
	}

	public void set(String arg)
	{
		this.arg = arg;
	}
	
	public String get()
	{
		return arg;
	}
	
	public int noArgs()
	{
		return 5;
	}
	
	public String doubleString(String arg)
	{
		System.out.println("doubleString('" + arg + "')");
		return arg + arg;
	}
}

