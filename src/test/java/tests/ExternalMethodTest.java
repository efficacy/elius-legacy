package tests;

import java.io.*;

import junit.framework.*;
import com.efsol.elius.*;
import com.efsol.elius.words.*;

public class ExternalMethodTest extends TestCase
{
	private ExternalMethodWord word;
	private Reader in;
	private Writer out;
	private Context context;
	private Target target;
	
	public ExternalMethodTest(String name)
	{
		super(name);
	}

	public void setUp()
	{
		word = new ExternalMethodWord();
		in = new StringReader("");
		out = new StringWriter();
		context = new BasicContext(in, out);
		target = new Target();
	}

	public void testNoArguments()
	{
		word.executeExternalMethod(context, target, "noArgs");
		assertEquals(1, context.size());
		assertEquals(5, context.popInt());

		word.executeExternalMethod(context, target, "nowt");
		assertEquals(0, context.size());
	}

	public void testSetGet()
	{
		word.executeExternalMethod(context, target, "set", new Object[] { "hello" });
		assertEquals(0, context.size());

		word.executeExternalMethod(context, target, "get");
		assertEquals(1, context.size());
		assertEquals("hello", context.popString());
	}

	public void testStringIndexOf()
	{
		word.executeExternalMethod(context, "hello", "indexOf", new Object[] { "ll" });
		assertEquals(1, context.size());
		assertEquals(new Integer(2), context.popContent());
	}
}
