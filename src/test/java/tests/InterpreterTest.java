package tests;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.efsol.elius.Interpreter;
import com.efsol.elius.LiteralToken;

import junit.framework.TestCase;

@SuppressWarnings({"rawtypes", "unchecked"})
public class InterpreterTest extends TestCase
{
	private Interpreter cli;
	private StringWriter out;

	public InterpreterTest(String name)
	{
		super(name);
	}

	@Override
	public void setUp()
	{
		cli = new Interpreter();
		out = new StringWriter();
	}

	private void run(String s)
	{
		cli.clear();
		out.getBuffer().setLength(0);
		cli.run(new StringReader(s), out);
	}

	public void testEmpty()
	{
		run("");
		assertEquals("empty command leaves an empty stack", 0, cli.size());
	}

	public void testComment()
	{
		run("()");
		assertEquals("empty comment leaves an empty stack", 0, cli.size());

		run("(1234)");
		assertEquals("one-word comment leaves an empty stack", 0, cli.size());

		run("(12 34)");
		assertEquals("two-word comment leaves an empty stack", 0, cli.size());

		run("(12\n34)");
		assertEquals("multi-line comment leaves an empty stack", 0, cli.size());

		run("() 1");
		assertEquals("empty comment ends properly", 1, cli.size());
		assertEquals("1 => 1", new Integer(1), cli.popContent());

		run("(12 34) 1");
		assertEquals("two-word comment ends properly", 1, cli.size());
		assertEquals("1 => 1", new Integer(1), cli.popContent());
	}

	public void testString()
	{
		run("''");
		assertEquals("empty string leaves a single-element stack", 1, cli.size());
		assertEquals("'' => ''", "", cli.popContent());

		run("'\\''");
		assertEquals("escaped quote string leaves a single-element stack", 1, cli.size());
		assertEquals("'\'' => '\''", "\'", cli.popContent());

		run("'1234'");
		assertEquals("one-word string leaves a single-element stack", 1, cli.size());
		assertEquals("'1234' => '1234'", "1234", cli.popContent());

		run("'12 34'");
		assertEquals("two-word string leaves a single-element stack", 1, cli.size());
		assertEquals("'12 34' => '12 34'", "12 34", cli.popContent());

		run("'12\n34'");
		assertEquals("multi-line string leaves a single-element stack", 1, cli.size());
		assertEquals("'12\n34' => '12\n34'", "12\n34", cli.popContent());

		run("'12 34' 1");
		assertEquals("two-word string ends properly", 2, cli.size());
		assertEquals("1 => 1", new Integer(1), cli.popContent());
		assertEquals("'12 34' => '12 34'", "12 34", cli.popContent());
	}

	public void testSingleNumber()
	{
		run("1");
		assertEquals("single number leaves a single-element stack", 1, cli.size());
		assertEquals("1 => 1", new Integer(1), cli.popContent());

		run("-1");
		assertEquals("negative number leaves a single-element stack", 1, cli.size());
		assertEquals("-1 => -1", new Integer(-1), cli.popContent());

		run(" [ 'hello' dup println ] '--' define --");
		assertEquals("defined '--' leaves a single-element stack", 1, cli.size());
		assertEquals(" [ 'hello' ] '--' define -- => 'hello'", "hello", cli.popContent());

		run(" [ 'hello' dup println ] '4-6' define 4-6");
		assertEquals("defined '4-6' leaves a single-element stack", 1, cli.size());
		assertEquals(" [ 'hello' ] '4-6' define 4-6 => 'hello'", "hello", cli.popContent());

		run(" [ 'hello' dup println ] '0x45' define 0x45");
		assertEquals("defined '0x45' leaves a single-element stack", 1, cli.size());
		assertEquals(" [ 'hello' ] '0x45' define 0x45 => 'hello'", "hello", cli.popContent());
	}

	public void testTwoNumbers()
	{
		run("1 2");
		assertEquals("two numbers leave a two-element stack", 2, cli.size());
		assertEquals("2 => 2", new Integer(2), cli.popContent());
		assertEquals("1 => 1", new Integer(1), cli.popContent());

		run("1\r\n\t2");
		assertEquals("two numbers leave a two-element stack", 2, cli.size());
		assertEquals("2 => 2", new Integer(2), cli.popContent());
		assertEquals("1 => 1", new Integer(1), cli.popContent());
	}

	public void testStringOps()
	{
		run("'hello,' ' world' $+");
		assertEquals("concat two strings leaves a one-element stack", 1, cli.size());
		assertEquals("'hello,' ' world' $+ => 'hello, world'", "hello, world", cli.popContent());

		List list = Arrays.asList(new Object[] {
			new LiteralToken("hello"),
			new LiteralToken("there"),
			new LiteralToken("world") } );

		run("'hello/there/world' '/' $split");
		assertEquals("split string leaves a one-element stack", 1, cli.size());
		assertEquals("'hello/there/world' '/' $split => ['hello' 'there' 'world']", list, cli.popContent());

		run("['hello' 'there' 'world'] ';' $join");
		assertEquals("join strings leaves a one-element stack", 1, cli.size());
		assertEquals("['hello' there' 'world'] ';' $join => 'hello;there;world'", "hello;there;world", cli.popContent());
	}

	public void testMath()
	{
		run("1 2 +");
		assertEquals("add two numbers leave a one-element stack", 1, cli.size());
		assertEquals("1 + 2 => 3", new Integer(3), cli.popContent());

		run("1 2 -");
		assertEquals("subtract two numbers leave a one-element stack", 1, cli.size());
		assertEquals("1 - 2 => -1", new Integer(-1), cli.popContent());
	}

	public void testExit()
	{
		run("exit");
		assertEquals("plain exit leaves an empty stack", 0, cli.size());

		run("exit 1");
		assertEquals("exit followed leaves an empty stack", 0, cli.size());

		run("1 exit");
		assertEquals("exit preceded leaves a single-item stack", 1, cli.size());
		assertEquals("1 => 1", new Integer(1), cli.popContent());
	}

	public void testPrint()
	{
		run("1 print");
		assertEquals("value printed leaves an empty stack", 0, cli.size());
		assertEquals("1 printed => 1", "1", out.toString());

		run("1 println");
		assertEquals("value printed leaves an empty stack", 0, cli.size());
		assertEquals("1 printed with newline => 1", "1\n", out.toString());
	}

	public void testList()
	{
		List ref = new ArrayList();
		List tmp;

		run("[]");
		assertEquals("empty array leaves a single-element stack", 1, cli.size());
		assertEquals("[] => []", ref, cli.popContent());

		run("[ ]");
		assertEquals("separated array leaves a single-element stack", 1, cli.size());
		assertEquals("[ ] => []", ref, cli.popContent());

		run("[ 12 ]");
		assertEquals("filled array leaves a single-element stack", 1, cli.size());
		ref.add(new LiteralToken(new Integer(12)));
		assertEquals("[ 12 ] => [ 12 ]", ref, cli.popContent());

		run("[ 12 34 ]");
		assertEquals("two filled array leaves a single-element stack", 1, cli.size());
		ref.add(new LiteralToken(new Integer(34)));
		assertEquals("[ 12 34 ] => [ 12 34 ]", ref, cli.popContent());

		run("[ 12 34 [ 56 78 ] ]");
		assertEquals("nested array leaves a single-element stack", 1, cli.size());
		List tmp1 = new ArrayList();
		tmp1.add(new LiteralToken(new Integer(56)));
		tmp1.add(new LiteralToken(new Integer(78)));
		ref.add(new LiteralToken(tmp1));

		tmp = (List)cli.popContent();
		assertEquals("[ 12 34 [ 56 78 ]] => [ 12 34 [ 56 78 ]]", ref, tmp);
		assertEquals(3, tmp.size());
		assertEquals(tmp1, ((LiteralToken)tmp.get(2)).getContent());
	}

	public void testDef()
	{
		run("[ 27 ] do");
		assertEquals("simple block execution leaves single-item stack", 1, cli.size());
		assertEquals("[ 27 ] do => 27", new Integer(27), cli.popContent());

		run("[ 27 4 + ] do");
		assertEquals("complex block execution leaves single-item stack", 1, cli.size());
		assertEquals("[ 27 4 + ] do => 31", new Integer(31), cli.popContent());

		run("[ 27 ] 'dopple' define");
		assertEquals("word definition leaves empty stack", 0, cli.size());

		run("dopple");
		assertEquals("running defined word leaves single-item stack", 1, cli.size());
		assertEquals("dopple => 27", new Integer(27), cli.popContent());

		run("dopple dopple");
		assertEquals("running defined word twice leaves two-item stack", 2, cli.size());
		assertEquals("dopple => 27", new Integer(27), cli.popContent());
		assertEquals("dopple => 27", new Integer(27), cli.popContent());

		run("[ 1 + ] 'inc' define");
		assertEquals("word definition leaves empty stack", 0, cli.size());

		run("106 inc");
		assertEquals("running defined word leaves single-item stack", 1, cli.size());
		assertEquals("106 inc => 107", new Integer(107), cli.popContent());

		run("'dopple' edit");
		assertEquals("editing defined word leaves single-item stack", 1, cli.size());
		assertEquals("'dopple' edit => [ 27 ]", "[ 27 ]", cli.popContent());

		run("'inc' edit");
		assertEquals("editing defined word leaves single-item stack", 1, cli.size());
		assertEquals("'inc' edit => [ 1 + ]", "[ 1 + ]", cli.popContent());

		run("[ 3 + ] 'inc' define");
		assertEquals("word definition leaves empty stack", 0, cli.size());

		run("106 inc");
		assertEquals("running defined word leaves single-item stack", 1, cli.size());
		assertEquals("106 inc => 109", new Integer(109), cli.popContent());

		run("'inc' edit");
		assertEquals("editing defined word leaves single-item stack", 1, cli.size());
		assertEquals("'inc' edit => [ 3 + ]", "[ 3 + ]", cli.popContent());

		run("[ 'hello' 'there' '/' $join ] 'ht' define");
		assertEquals("word definition leaves empty stack", 0, cli.size());

		run("'ht' edit");
		assertEquals("editing defined word leaves single-item stack", 1, cli.size());
		assertEquals("'ht' edit => [ 'hello' 'there' '/' $join ]", "[ 'hello' 'there' '/' $join ]", cli.popContent());

		run("[ [ 'hello' 'there' ] '/' $join ] 'ht' define");
		assertEquals("word definition leaves empty stack", 0, cli.size());

		run("'ht' edit");
		assertEquals("editing defined word leaves single-item stack", 1, cli.size());
		assertEquals("'ht' edit => [ [ 'hello' 'there' ] '/' $join ]", "[ [ 'hello' 'there' ] '/' $join ]", cli.popContent());
	}

	public void testVars()
	{
		run("27 'xx' set");
		assertEquals("variable set leaves empty stack", 0, cli.size());

		run("'xx' get");
		assertEquals("variable get leaves single-item stack", 1, cli.size());
		assertEquals("'xx' get => 27", new Integer(27), cli.popContent());
	}

	public void testNative()
	{
		run("'hello' 'length' native");
		assertEquals("call String length returns 1 item", 1, cli.size());
		assertEquals("'hello' 'length' native => 5", new Integer(5), cli.popContent());

		run("'ll' 'hello' 'indexOf' arg_native");
		assertEquals("call String indexOf returns 1 item", 1, cli.size());
		assertEquals("'ll' 'hello' 'indexOf' arg_native => 2", new Integer(2), cli.popContent());

		run("[ 'll' ] 'hello' 'indexOf' arg_native");
		assertEquals("call String indexOf returns 1 item", 1, cli.size());
		assertEquals("[ 'll' ] 'hello' 'indexOf' arg_native => 2", new Integer(2), cli.popContent());

		run("1 'hello' 'charAt' arg_native");
		assertEquals("call String charAt returns 1 item", 1, cli.size());
		assertEquals("1 'hello' 'charAt' arg_native => 'e'", "e", cli.popString());

	}

	public void testArrayWords()
	{
		run("[ 1 2 ] explode");
		assertEquals("array explode returns 2 items", 2, cli.size());
		assertEquals("top array element => 2", new Integer(2), cli.popContent());
		assertEquals("top array element => 1", new Integer(1), cli.popContent());

		List list = Arrays.asList(new Object[] {
			new LiteralToken(new Integer(1)),
			new LiteralToken(new Integer(2)) } );

		run("1 2 2 implode");
		assertEquals("array implode returns 1 item", 1, cli.size());
		assertEquals("new list => [ 1 2 ]", list, cli.popContent());
	}

	public void testStackWords()
	{
		run("1 dup");
		assertEquals("dup returns 2 items", 2, cli.size());
		assertEquals("top array element => 1", new Integer(1), cli.popContent());
		assertEquals("top array element => 1", new Integer(1), cli.popContent());

		run("1 2 swap");
		assertEquals("swap returns 2 items", 2, cli.size());
		assertEquals("top array element => 1", new Integer(1), cli.popContent());
		assertEquals("top array element => 2", new Integer(2), cli.popContent());

		run("1 2 drop");
		assertEquals("drop returns 1 items", 1, cli.size());
		assertEquals("top array element => 1", new Integer(1), cli.popContent());

		run("1 2 3 3 roll");
		assertEquals("roll returns 3 items", 3, cli.size());
		assertEquals("top array element => 1", new Integer(1), cli.popContent());
		assertEquals("top array element => 3", new Integer(3), cli.popContent());
		assertEquals("top array element => 2", new Integer(2), cli.popContent());

		run("1 2 3 -3 roll");
		assertEquals("roll returns 3 items", 3, cli.size());
		assertEquals("top array element => 2", new Integer(2), cli.popContent());
		assertEquals("top array element => 1", new Integer(1), cli.popContent());
		assertEquals("top array element => 3", new Integer(3), cli.popContent());
	}

	public void testIO()
	{
		String file = "'src/test/output/ugh2.txt'";
		run(file + " readfile");
		assertEquals("read(missing)file leaves single item stack", 1, cli.size());
		assertEquals("read(missing)file => ''", "", cli.popString());

		run("'huh, what' " + file + " writefile");
		assertEquals("writefile leaves empty item stack", 0, cli.size());

		run(file + " readfile");
		assertEquals("readfile leaves single item stack", 1, cli.size());
		assertEquals("readfile => 'huh, what'", "huh, what", cli.popString());

		run(file + " dropfile");

		run(file + " readfile");
		assertEquals("read(missing)file leaves single item stack", 1, cli.size());
		assertEquals("read(missing)file => ''", "", cli.popString());
	}

}
